from django.contrib import admin

# Register your models here.
from books.models import Book


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'description', 'author', 'editorial', 'publication_date')
    search_fields = ('id', 'title', 'author', 'editorial', 'publication_date')
