from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, DeleteView

from books.forms import BookForm
from books.models import Book


class Home(TemplateView):
    template_name = 'home.html'


class AddBookView(CreateView):
    model = Book
    form_class = BookForm
    template_name = 'books/register_book.html'
    success_url = '/'


class UpdateBookView(UpdateView):
    model = Book
    form_class = BookForm
    pk_url_kwarg = 'pk'
    template_name = 'books/register_book.html'
    success_url = '/'


class ListBookView(ListView):
    model = Book
    queryset = Book.objects.all()
    template_name = 'books/list_books.html'
    context_object_name = 'book_list'


class DeleteBookView(DeleteView):
    model = Book
    pk_url_kwarg = 'pk'
    template_name = 'books/delete_book.html'
    success_url = '/'
