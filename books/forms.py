from django import forms
from django.forms import DateInput

from books.models import Book


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = '__all__'
        widgets = {
            'publication_date': DateInput(attrs={
                'class': "form-control mb-3",
                'style': 'max-width: 500px;',
                'placeholder': '',
                'type': 'date',
            }),
        }


