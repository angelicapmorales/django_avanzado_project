from django.contrib.auth.decorators import login_required
from django.urls import path

from books.views import Home, AddBookView, UpdateBookView, ListBookView, DeleteBookView

urlpatterns = [
    path('', Home.as_view(), name='Home'),
    path('add_book', login_required(AddBookView.as_view()), name='add_book'),
    path('update_book/<int:pk>/', login_required(UpdateBookView.as_view()), name='update_book'),
    path('list_book', login_required(ListBookView.as_view()), name='list_book'),
    path('delete_book/<int:pk>/', login_required(DeleteBookView.as_view()), name='delete_book'),

]