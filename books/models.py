from django.db import models


# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=200, null=False)
    description = models.TextField(null=False)
    author = models.CharField(max_length=150, null=False)
    editorial = models.CharField(max_length=200, null=False)
    publication_date = models.DateField()

    def __str__(self):
        return self.title
