from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import FormView, CreateView, RedirectView

from user.forms import LoginForm, SignupForm
from user.models import User


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'user/login.html'
    success_url = '/'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)


class SignupView(CreateView):
    form_class = SignupForm
    model = User
    template_name = 'user/signup.html'
    success_url = '/'


class LogoutView(RedirectView):
    url = reverse_lazy('Home')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)
