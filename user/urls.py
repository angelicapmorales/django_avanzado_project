from django.contrib.auth.decorators import login_required
from django.urls import path

from user.views import LoginView, SignupView, LogoutView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('signup', SignupView.as_view(), name='signup'),
    path('logout/', login_required(LogoutView.as_view()), name='logout')
]