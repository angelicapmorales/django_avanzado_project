from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django import forms
from django.forms import ModelForm

from user.models import User


class LoginForm(AuthenticationForm):
    pass


class SignupForm(UserCreationForm):
    email = forms.EmailField(label='email')
    first_name = forms.CharField(label='first name')
    last_name = forms.CharField(label='last name')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')
