from django.contrib import admin

# Register your models here.
from user.models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'identification', 'username', 'email', 'first_name', 'last_name')
    search_fields = ('id', 'identification', 'username', 'email', 'first_name', 'last_name')
