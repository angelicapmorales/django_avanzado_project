## Installation

1. `pip install -r requirements.txt`
2. `python manage.py migrate`

## Running the Project

1. `python manage.py runserver`

## More instructions

You first need to register a user in the navbar option "sign up", then you can log in with the user created in "login".
This will take you to the "add books" and "list book" part (you will only access these options if you are logged in), here you can register a book by filling in the form fields. In listing books you will have the option to edit and/or delete a book record. In this part you will also see in the navbar on the right the user who is logged in and next to it the "logout" option that will allow you to end the session.
